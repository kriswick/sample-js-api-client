describe('When user clicks a calculator number button', function(){

  it('should update the calculator op1 and display', function(){
  	Calculator.clear();
  	Calculator.buttonPressed('2');
    
    expect(Calculator.op1).toEqual('2');
    expect(Calculator.op2).toEqual(null);
    expect(document.getElementById('workingCalculation').innerHTML).toEqual('2');
  });

  it('should append to the calculator op1 if op1 is already populated', function(){
  	Calculator.clear();
  	Calculator.op1 = '1'
  	Calculator.buttonPressed('2');
    
    expect(Calculator.op1).toEqual('12');
    expect(Calculator.op2).toEqual(null);
    expect(document.getElementById('workingCalculation').innerHTML).toEqual('12');
  });

  it('should update op2 if op1 is already populated and operation is set', function(){
  	Calculator.op1 = '1'
  	Calculator.operation = 'add';
  	Calculator.buttonPressed('2');
    
    expect(Calculator.op1).toEqual('1');
    expect(Calculator.op2).toEqual('2');
    expect(document.getElementById('workingCalculation').innerHTML).toEqual('1 + 2');
  });

});

describe('When user clicks a calculator operation button', function(){

	it('should set the calculator operation', function(){
		Calculator.clear();
		Calculator.op1 = '1';
		Calculator.buttonPressed('+');

		expect(Calculator.op1).toEqual('1');
    	expect(Calculator.op2).toEqual(null);
    	expect(Calculator.operation).toEqual('add');
    	expect(document.getElementById('workingCalculation').innerHTML).toEqual('1 + ');
	});

	it('should not set the calculator operation if op1 is null', function(){
		Calculator.clear();
		Calculator.buttonPressed('+');

		expect(Calculator.op1).toEqual(null);
    	expect(Calculator.op2).toEqual(null);
    	expect(Calculator.operation).toEqual(null);
    	expect(document.getElementById('results').innerHTML).toEqual('ERROR');
	});
});
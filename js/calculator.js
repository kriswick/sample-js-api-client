var Calculator = {

	workingCalculation : null,
	result : null,
	op1 : null,
	op2 : null,
	operation: null,

	buttonPressed: function(key){
		switch(key){
			case 'C': 
				Calculator.clearAndUpdateDisplay();
				break;
			case '=':
				Calculator.execute();
				break;
			case '\u002B':
				Calculator.operationKeyPressed('add');
				break;
			case '\u2212':
				Calculator.operationKeyPressed('subtract');
				break;
			case '\u00D7':
				Calculator.operationKeyPressed('multiply');
				break;
			case '\u00F7':
				Calculator.operationKeyPressed('divide');
				break;
			case 'X^Y':
				Calculator.operationKeyPressed('power');
				break;
			case 'SQRT(X)':
				Calculator.operationKeyPressed('square_root');
				break;
			case 'LOG(X)':
				Calculator.operationKeyPressed('log10');
				break;
			case 'Ln(X)':
				Calculator.operationKeyPressed('ln');
				break;
			case 'PI':
				Calculator.operationKeyPressed('pi');
				break;
			case 'E':
				Calculator.operationKeyPressed('e');
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '.':
				Calculator.numberKeyPressed(key);
				break;
			default:
				console.log('unknown key', key);
		};
	},

	operationKeyPressed: function(op){
		if (Calculator.result !== null){
			Calculator.clearAndUpdateDisplay();
		}
		if (Calculator.op1 === null){
			Calculator.reportError('ERROR');
		} else {
			Calculator.operation = op;
			Calculator.updateDisplay();
		}
	},

	numberKeyPressed: function(num){

		function appendNum(num, to){
			if (to == null){
				return num;
			} else {
				return to + num; //Strings
			}
		}


		if (Calculator.result !== null){
			Calculator.clearAndUpdateDisplay();
		}

		if(Calculator.operation == null){
			Calculator.op1 = appendNum(num, Calculator.op1);
		} else {
			Calculator.op2 = appendNum(num, Calculator.op2);
		};

		Calculator.updateDisplay();
	},

	initButtons : function(){
		let buttons = document.querySelectorAll('.btn');
		
		buttons.forEach(function(btn){
			btn.addEventListener("click", function(){
				Calculator.buttonPressed(btn.innerHTML);
			});
		});
	},

	clear: function(){
		Calculator.op1 = null;
		Calculator.op2 = null;
		Calculator.operation = null;
		Calculator.result = null;
		Calculator.workingCalculation = null;
	},

	clearAndUpdateDisplay: function(){
		Calculator.clear();
		Calculator.updateDisplay();
	},

	updateDisplay: function(){
		function nullEscape(str){
			if (str == null ){
				return '';
			} else {
				return str;
			}
		}

		function twoOperandOpDisplay(op1, op2, operation){
			let operations = {
				'add' : ' + ',
				'subtract' : ' - ',
				'divide' : ' \u00F7 ',
				'multiply': ' \u00D7 ',
				'power' : ' ^ '
			}
			return op1 + operations[operation] + nullEscape(op2);
		}

		function oneOperandOpDisplay(op1, operation){
			let operations = {
				'square_root': 'SQRT',
				'log10': 'LOG',
				'ln': 'Ln'
			}
			return operations[operation] + "(" + op1 + ")";
		}

		if (Calculator.operation == 'pi'){
			Calculator.workingCalculation = 'PI';
		} else if (Calculator.operation == 'e'){
			Calculator.workingCalculation = 'e';
		} else if(Calculator.op1 !== null && Calculator.operation !== null){
			switch(Calculator.operation){
				case 'add':
				case 'subtract':
				case 'multiply':
				case 'divide':
				case 'power':
						Calculator.workingCalculation = twoOperandOpDisplay(Calculator.op1, Calculator.op2, Calculator.operation);
						break;
				case 'square_root':
				case 'log10':
				case 'ln':
						Calculator.workingCalculation = oneOperandOpDisplay(Calculator.op1, Calculator.operation);
						break;
			};
		} else if (Calculator.operation === null && Calculator.op1 !== null) {
			Calculator.workingCalculation = Calculator.op1;
		}
		
		document.getElementById('workingCalculation').innerHTML = Calculator.workingCalculation;
		document.getElementById('results').innerHTML = Calculator.result;
	},

	reportError: function(msg){
		Calculator.result = msg;
		Calculator.updateDisplay();		
	},

	execute: function(){
		let params = '';
		try {
			switch(Calculator.operation){
				case 'add':
				case 'subtract':
				case 'multiply':
				case 'divide':
				case 'power':
						if (Calculator.op1 === null || Calculator.op2 === null){
							throw 'ERROR';	
						} else {
							params = '?op1=' + Calculator.op1 + '&op2=' + Calculator.op2;
						}
						break;
				case 'square_root':
				case 'log10':
				case 'ln':
						if (Calculator.op1 === null){
							throw 'ERROR';	
						} else {
							params = '?op1=' + Calculator.op1;
						}
						break;
				case null: throw 'ERROR';
			};
			let link = "http://calctest.iesim.biz/" + Calculator.operation + params;
			fetch(link, { 
				headers: { 
					"Accept": "application/json",
				}})
			    .then(res => res.json()) 
			    .then(response => {
			        Calculator.result = response.result;
			        Calculator.updateDisplay();
			    })
			    .catch(err => {
			        
			    });
		 } catch (e) {
		 	console.log('Error', e);
		 	Calculator.reportError(e);
		 }
	}

};


(function(exports, document){
	
		function initCalculator(){

			function onReady(event){
				document.removeEventListener("DOMContentLoaded", onReady);
				console.log('Initializing calculator....');
				Calculator.initButtons();
				Calculator.clearAndUpdateDisplay();
			}

			document.addEventListener("DOMContentLoaded", onReady);
		}

		exports.initCalculator = initCalculator;
})(window, document);
This is a simple implementation of a calculator client for http://calctest.iesim.biz/ service.

Note that this client uses no external Javascript library except for Jasmine (https://jasmine.github.io/) for testing. It uses basic HTML5 and ECMAScript5 functionality.

It does use Bootstrap CSS for styling (https://getbootstrap.com)

This client was tested on Google Chrome (Version 68) successfully. It may not work correctly in IE (due to the use of fetch api of Javascript); It was tested on Tomcat (version 8.0.53) as a webserver. 